// Import des dépendances
const express = require("express");

// Instanciation d'un objet express
const app = express();

// Port écouté par le serveur
const port = 3000;

// *** Routes (endpoints) **

app.get("/", (req, res) => {
    res.send("Server OK !!!");
})

app.get("/square/:num", (req, res) => {
	let n = parseInt(req.params.num);
    let square = n * n;
    res.json({ in: n, out: square });
})

app.listen(port, () => console.log(`Server started on port ${port}...`));